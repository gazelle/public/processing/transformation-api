package net.ihe.gazelle.transformation.service;

import javax.xml.ws.WebFault;

/**
 * Exception use to handle Daffodil compilation errors with the full diagnotic information
 *
 * @author ceoche
 */
@WebFault(name = "CompilationException", targetNamespace = "http://ws.transformation.gazelle.ihe.net/")
public class CompilationException extends Exception {

    private static final long serialVersionUID = 5612782505318231058L;

    public CompilationException(String message) {
        super(message);
    }

    public CompilationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
