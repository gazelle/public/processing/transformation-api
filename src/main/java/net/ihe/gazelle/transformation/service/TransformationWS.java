package net.ihe.gazelle.transformation.service;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import java.io.IOException;
import java.util.List;

/**
 * The Gazelle Transformation Service can transform any set of parsable raw data (plain text, binary, etc.) into a structured XML according to a
 * Daffodil DFDL Schema. The transformation can also be reversed (XML to data) using the same schema.
 * <p>
 * See <a href="https://en.wikipedia.org/wiki/Data_Format_Description_Language">the Data Format Description Language</a> for more information.
 *
 * @author ceoche
 */
@Local
@WebService(name = "Transformation", targetNamespace = "http://ws.transformation.gazelle.ihe.net/", serviceName = "GazelleTransformationService")
public interface TransformationWS {

    /**
     * Transform XML into data using the provided  DFDL XSD
     *
     * @param dfdlSchema     DFDL XSD Schema for the transformation (bytes of UTF-8 String)
     * @param xmlToTransform XML (bytes of UTF-8 String) that will be transformed into data accrording to the schema
     * @return transformed data in byte array (the way those bytes are read depends on the given schema)
     * @throws IllegalArgumentException if dfdlSchema or xmlToTransform is null or empty
     * @throws CompilationException     if an error occured while compiling the schema
     * @throws TransformationException  if an error occured while performing the transformation
     */
    @WebMethod
    @WebResult(name = "transformedData")
    byte[] transformXMLGivenDfdlSchema(@WebParam(name = "dfdlSchema") @XmlElement(required = true) byte[] dfdlSchema,
                                       @WebParam(name = "xmlToTransform") @XmlElement(required = true) byte[] xmlToTransform)
            throws IllegalArgumentException, CompilationException, TransformationException;

    /**
     * Transform XML into data using the referenced DFDL Schema
     *
     * @param dfdlSchemaKeyword Keyword of the DFDL Schema to use for the transformation.
     * @param xmlToTransform    XML (bytes of UTF-8 String) that will be transformed into data according to the schema
     * @return transformed data in byte array (the way those bytes are read depends on the given schema) alongs with information on the schema used.
     * @throws IllegalArgumentException if dfdlSchemaKeyword or xmlToTransform is null or empty
     * @throws ObjectNotFoundException  if the given keyword does not match any DFDL Schema
     * @throws CompilationException     if an error occured while compiling the schema
     * @throws TransformationException  if an error occured while performing the transformation
     */
    @WebMethod
    @WebResult(name = "transformedWithDFDLReference")
    TransformedDataWithDFDLReference transformXMLGivenReferenceToDfdlSchema(
            @WebParam(name = "dfdlSchemaKeyword") @XmlElement(required = true) String dfdlSchemaKeyword,
            @WebParam(name = "xmlToTransform") @XmlElement(required = true) byte[] xmlToTransform)
            throws IllegalArgumentException, ObjectNotFoundException, CompilationException, TransformationException;

    /**
     * Transform data into XML using the provided  DFDL XSD
     *
     * @param dfdlSchema      DFDL XSD Schema for the transformation (bytes of UTF-8 String)
     * @param dataToTransform raw byte array that will be transformed into XML accrording to the schema
     * @return transformed XML (bytes of UTF-8 String)
     * @throws IllegalArgumentException if dfdlSchema or dataToTransform is null or empty
     * @throws CompilationException     if an error occured while compiling the schema
     * @throws TransformationException  if an error occured while performing the transformation
     */
    @WebMethod
    @WebResult(name = "transformedXml")
    byte[] transformDataGivenDfdlSchema(@WebParam(name = "dfdlSchema") @XmlElement(required = true) byte[] dfdlSchema,
                                        @WebParam(name = "dataToTransform") @XmlElement(required = true) byte[] dataToTransform)
            throws IllegalArgumentException, CompilationException, TransformationException;

    /**
     * Transform data into XML using the referenced DFDL Schema
     *
     * @param dfdlSchemaKeyword Keyword of the DFDL Schema to use for the transformation.
     * @param dataToTransform   raw byte array that will be transformed into XML accrording to the schema
     * @return transformed XML (bytes of UTF-8 String) alongs with information on the schema used.
     * @throws IllegalArgumentException if dfdlSchemaKeyword or dataToTransform is null or empty
     * @throws ObjectNotFoundException  if the given keyword does not match any DFDL Schema
     * @throws CompilationException     if an error occured while compiling the schema
     * @throws TransformationException  if an error occured while performing the transformation
     */
    @WebMethod
    @WebResult(name = "transformedWithDFDLReference")
    TransformedXmlWithDFDLReference transformDataGivenReferenceToDfdlSchema(
            @WebParam(name = "dfdlSchemaKeyword") @XmlElement(required = true) String dfdlSchemaKeyword,
            @WebParam(name = "dataToTransform") @XmlElement(required = true) byte[] dataToTransform)
            throws IllegalArgumentException, ObjectNotFoundException, CompilationException, TransformationException;

    /**
     * Get available DFDL schemas recorded in this Gazelle Transformation application
     *
     * @return A list of DFDL schemas
     * @throws ObjectNotFoundException if no DFDL schema is defined
     */
    @WebMethod
    @WebResult(name = "dfdlSchema")
    List<DFDLSchemaEntry> getAvailableDfdlSchemas() throws ObjectNotFoundException;

    /**
     * Get available object types recorded in this Gazelle Transformation application
     *
     * @return A list of object types
     * @throws ObjectNotFoundException if no object types are defined
     */
    @WebMethod
    @WebResult(name = "objectType")
    List<ObjectTypeEntry> getAvailableObjectTypes() throws ObjectNotFoundException;

    /**
     * Get available DFDL Schemas recorded in this Gazelle Transformation application for a given object type.
     *
     * @param objectTypeKeyword object type keyword to filter DFDL Schemas with
     * @return A list of all DFDL schemas having the requested object type
     * @throws ObjectNotFoundException  if no dfdl schema found for given object type keyword
     * @throws IllegalArgumentException if objectTypeKeyword is null or empty
     */
    @WebMethod
    @WebResult(name = "dfdlSchema")
    List<DFDLSchemaEntry> getDfdlSchemasForAGivenObjectType(
            @WebParam(name = "objectTypeKeyword") @XmlElement(required = true) String objectTypeKeyword)
            throws ObjectNotFoundException, IllegalArgumentException;

    /**
     * Get DFDL Schema XSD content
     *
     * @param dfdlSchemaKeyword Keyword of the DFDL schema requested
     * @return The DFDL XSD schema content (bytes of UTF-8 String)
     * @throws ObjectNotFoundException  if no dfdl schema match the given keyword
     * @throws IOException              in case of DFDL file access error
     * @throws IllegalArgumentException if dfdlSchemaKeyword is null or empty
     */
    @WebMethod
    @WebResult(name = "dfdlSchemaContent")
    byte[] getDfdlSchemaContentByKeyword(@WebParam(name = "dfdlSchemaKeyword") @XmlElement(required = true) String dfdlSchemaKeyword)
            throws ObjectNotFoundException, IOException, IllegalArgumentException;

}
