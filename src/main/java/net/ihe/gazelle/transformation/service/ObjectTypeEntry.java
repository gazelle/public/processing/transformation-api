package net.ihe.gazelle.transformation.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

/**
 * Reprensent an object type entry recorded in Gazelle Transformation application.
 * <em>This is a minimalist version of the real entity.</em>
 *
 * @author ceoche
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectTypeEntry {

    @XmlAttribute(name = "keyword")
    private String keyword;

    @XmlAttribute(name = "lastChanged")
    private Date lastChanged;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "descrption")
    private String description;

    ObjectTypeEntry() {
    }

    public ObjectTypeEntry(String keyword, String name, Date lastChanged, String description) {
        this.keyword = keyword;
        this.lastChanged = (Date) lastChanged.clone();
        this.name = name;
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public Date getLastChanged() {
        return (Date) lastChanged.clone();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
