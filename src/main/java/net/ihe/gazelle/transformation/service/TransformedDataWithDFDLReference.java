package net.ihe.gazelle.transformation.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;

/**
 * Return transformation result with DFDL Schema information. This response is send when requesting a transformation with a reference to a recorded
 * DFDL Schema to keep track of which version as been used.
 *
 * @author ceoche
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TransformedDataWithDFDLReference {

    @XmlElement(name = "transformedData")
    private byte[] transformedData;

    @XmlElement(name = "dfdlSchema")
    private DFDLSchemaEntry dfdlSchema;

    TransformedDataWithDFDLReference() {
    }

    public TransformedDataWithDFDLReference(byte[] transformedData, DFDLSchemaEntry dfdlSchema) {
        this.dfdlSchema = dfdlSchema;
        this.transformedData = Arrays.copyOf(transformedData, transformedData.length);
    }

    public byte[] getTransformedData() {
        return Arrays.copyOf(transformedData, transformedData.length);
    }

    public DFDLSchemaEntry getDfdlSchema() {
        return dfdlSchema;
    }
}
