package net.ihe.gazelle.transformation.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

/**
 * Reprensent a DFDL Schema entry recorded in Gazelle Transformation application.
 * <em>This is a minimalist version of the real entity.</em>
 *
 * @author ceoche
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DFDLSchemaEntry {

    @XmlAttribute(name = "keyword")
    private String keyword;

    @XmlAttribute(name = "version")
    private String version;

    @XmlAttribute(name = "lastChanged")
    private Date lastChanged;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "objectType")
    private ObjectTypeEntry objectType;

    DFDLSchemaEntry() {
    }

    public DFDLSchemaEntry(String keyword, String name, String version, Date lastChanged, String description,
                           ObjectTypeEntry objectType) {
        this.keyword = keyword;
        this.name = name;
        this.version = version;
        this.lastChanged = (Date) lastChanged.clone();
        this.description = description;
        this.objectType = objectType;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public Date getLastChanged() {
        return (Date) lastChanged.clone();
    }

    public String getDescription() {
        return description;
    }

    public ObjectTypeEntry getObjectType() {
        return objectType;
    }
}
