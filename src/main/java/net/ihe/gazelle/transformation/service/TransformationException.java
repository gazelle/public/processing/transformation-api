package net.ihe.gazelle.transformation.service;

import javax.xml.ws.WebFault;
import java.util.Arrays;

/**
 * Exception use to handle Daffodil transformation errors with the full diagnotic information and eventually the partially transformed result
 *
 * @author ceoche
 */
@WebFault(name = "TransformationException", targetNamespace = "http://ws.transformation.gazelle.ihe.net/")
public class TransformationException extends Exception {

    private static final long serialVersionUID = 6161006800986760328L;

    private byte[] incompleteTransformation = null;

    public TransformationException(String message) {
        super(message);
    }

    public TransformationException(String message, byte[] incompleteTransformation) {
        super(message);
        this.incompleteTransformation = Arrays.copyOf(incompleteTransformation, incompleteTransformation.length);
    }

    public byte[] getIncompleteTransformation() {
        if (incompleteTransformation != null) {
            return Arrays.copyOf(incompleteTransformation, incompleteTransformation.length);
        } else {
            return null;
        }
    }

}
