package net.ihe.gazelle.transformation.service;

import javax.xml.ws.WebFault;

/**
 * Exception throwed when a requested entity is not found.
 *
 * @author ceoche
 */
@WebFault(name = "ObjectNotFoundException", targetNamespace = "http://ws.transformation.gazelle.ihe.net/")
public class ObjectNotFoundException extends Exception {
    private static final long serialVersionUID = -1975012188405993764L;

    public ObjectNotFoundException() {
    }

    public ObjectNotFoundException(String s) {
        super(s);
    }

    public ObjectNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ObjectNotFoundException(Throwable throwable) {
        super(throwable);
    }

    public ObjectNotFoundException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
