package net.ihe.gazelle.transformation.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;

/**
 * Return transformation result with DFDL Schema information. This response is send when requesting a transformation with a reference to a recorded
 * DFDL Schema to keep track of which version as been used.
 *
 * @author ceoche
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TransformedXmlWithDFDLReference {

    @XmlElement(name = "transformedXml")
    private byte[] transformedXml;

    @XmlElement(name = "dfdlSchema")
    private DFDLSchemaEntry dfdlSchema;

    TransformedXmlWithDFDLReference() {
    }

    public TransformedXmlWithDFDLReference(byte[] transformedXml, DFDLSchemaEntry dfdlSchema) {
        this.dfdlSchema = dfdlSchema;
        this.transformedXml = Arrays.copyOf(transformedXml, transformedXml.length);
    }

    public byte[] getTransformedXml() {
        return Arrays.copyOf(transformedXml, transformedXml.length);
    }

    public DFDLSchemaEntry getDfdlSchema() {
        return dfdlSchema;
    }
}
